# TP6 - Programmation d'applications client/serveur avec les sockets

On vous fournit deux exemples de code, l'un en C et l'autre en Java.
Ils sont à votre disposition dans ce dépôt https://gitlab.ensimag.fr/reseaux/tp6

Ce sont deux squelettes très basiques en C pour UDP permettant d'avoir une idée de l'utilisation des différents appels systèmes, ainsi qu'un exemple en Java pour TCP.

Pour compiler en activant la macro `debug` :
```console
$ CFLAGS=-DDEBUG make
gcc -DDEBUG -Wall    squdpclient.c   -o squdpclient
gcc -DDEBUG -Wall    squdpserv.c   -o squdpserv
gcc -DDEBUG -Wall    sqtcpserv.c   -o sqtcpserv
gcc -DDEBUG -Wall    sqtcpclient.c   -o sqtcpclient
gcc -DDEBUG -Wall    sqtcpselect.c   -o sqtcpselect
javac SqTCPServ.java
```

Remarque : utilisez des outils réseau existants pour tester votre code durant le développement.
Tester à la fois un client et un serveur en développement complique la recherche des problèmes.
Utilisez [`socklab`](https://github.com/drakkar-lig/socklab) que vous connaissez ou `nc`, `netcat`, `ncat`, `socat`, etc. des outils qu'il est indispensable de connaitre.

## Prendre en main le code

Compilez ces exemples puis essayez de vous connecter à l'aide d'un outil évoqué plus haut sur le serveur UDP.
Envoyez une requête et regardez ce qu'il se passe.

## Répondre côté serveur

Réalisez d'abord une version non connectée d'un client et d'un serveur permettant de calculer le carré de nombres entiers en C.
Pour commencer le plus simple est d'adapter le serveur UDP fourni en C pour qu'il réponde à un outil existant.
Une fois le serveur testé, codez le client.

En C, la principale difficulté consiste à remplir correctement les structures `struct sockaddr_in`.

## Client/serveur en mode connecté

Adaptez maintenant cette version le plus simplement possible au mode connecté.

Commencez par une structure la plus simple possible, en ne gérant qu'une connexion à la fois.

## Gestion de clients multiples

Modifiez la version connectée afin de gérer plusieurs clients simultanément.

### Utilisation de `select`

Exemple d'utilisation :
```c
   #ifndef FD_COPY
   #define FD_COPY(orig,dest) memcpy((dest),(orig),sizeof(*(dest)))
   #endif

   FD_ZERO(&rd_mask);
   FD_SET(passive_sock, &rd_mask);
   FD_ZERO(&wr_mask);
   FD_ZERO(&ex_mask);
   FD_SET(passive_sock, &ex_mask);

   max_fd = FD_SETSIZE; // should be max fd value, this works

   while (1) {
      FD_COPY(&rd_mask, &rd_sel);
      FD_COPY(&wr_mask, &wr_sel);
      FD_COPY(&ex_mask, &ex_sel);
      nb_set = select(max_fd, &rd_sel, &wr_sel, &ex_sel, NULL);
      if (nb_set < 0) {
          perror("select");
          // probably return or exit
      }
      if (nb_set > 0) {
         if (FD_ISSET(passive_sock, &rd_sel)) {
            // accept new_sock...
            FD_SET(new_sock, &rd_mask);
            FD_SET(new_sock, &wr_mask);
            FD_SET(new_sock, &ex_mask);
         }
         // do some work on newly created sockets, opened files, etc.
      }
   }
```

### Version Java

Faites de même en Java, cette fois en créant un `thread` de traitement pour chaque nouveau client.


## Conseils

- Pour débugger, le mieux est d'avoir une machine cible sous FreeBSD, sur laquelle vous pouvez lancer `socklab` et `tcpdump` ou `wireshark`.
Sinon récupérez et compilez `socklab` sous Linux, mais vous n'y êtes pas utilisateur privilégié.
- Faites bien attention à initialiser correctement les champs des structures utilisées (`memset`).
- Attention à ne pas juste affecter les pointeurs de structures mais à bien copier leur contenu (`memcpy`), par exemple pour la récupération de l'adresse IP.
- Attention au _byte order_ : toujours (ou presque, c'est indiqué dans les pages `man`) utiliser les macros de conversion _host to net_ lors du remplissage des structures réseau, et inversement (`htonl`, etc.).
- **Tester les valeurs de retour des appels systèmes**, en cas d'échec utiliser `perror` pour afficher un message personnalisé décrivant le type d'échec dont il s'agit (par exemple, impossible de créer la socket). Se référer au `man`, section `RETURN VALUES`.
- Parcourir les pages `man` indiquées ci-dessous, au moins pour repérer les fonctions intéressantes.
- Pour mieux comprendre les appels bloquants/non-bloquants voir `O_NONBLOCK` dans `fcntl` et `EWOULDBLOCK` dans plusieurs des pages man (il peut être prudent de positionner `O_NONBLOCK` sur les sockets qui ne devraient pas bloquer après `select`).
- Voir aussi `SO_NREAD` et `SO_NWRITE` dans `get/setsockopt`.

## Références

Voir les manuels Unix suivants et se souvenir qu'une section très intéressante des manuels est _see also_, généralement située vers la fin.

- [`netintro`](https://www.freebsd.org/cgi/man.cgi?query=netintro) (uniquement BSD), `ip` (section 7 sous Linux), [`tcp`](https://www.freebsd.org/cgi/man.cgi?query=tcp&sektion=4), [`udp`](https://www.freebsd.org/cgi/man.cgi?query=udp&sektion=4)
- [`socket`](https://www.freebsd.org/cgi/man.cgi?query=socket&sektion=2) (sections 2 et 7 sous Linux), [`bind`](https://www.freebsd.org/cgi/man.cgi?query=bind&sektion=2), [`listen`](https://www.freebsd.org/cgi/man.cgi?query=listen&sektion=2), [`accept`](https://www.freebsd.org/cgi/man.cgi?query=accept&sektion=2), [`connect`](https://www.freebsd.org/cgi/man.cgi?query=connect&sektion=2), [`get/setsockopt`](https://www.freebsd.org/cgi/man.cgi?query=setsockopt&sektion=2)
- [`read`](https://www.freebsd.org/cgi/man.cgi?query=read&sektion=2), [`write`](https://www.freebsd.org/cgi/man.cgi?query=write&sektion=2), [`recv`](https://www.freebsd.org/cgi/man.cgi?query=recv&sektion=2), [`recvfrom`](https://www.freebsd.org/cgi/man.cgi?query=recvfrom&sektion=2), [`send`](https://www.freebsd.org/cgi/man.cgi?query=send&sektion=2), [`sendto`](https://www.freebsd.org/cgi/man.cgi?query=sendto&sektion=2), [`fcntl`](https://www.freebsd.org/cgi/man.cgi?query=fcntl&sektion=2)
- [`select`](https://www.freebsd.org/cgi/man.cgi?query=select&sektion=2), [`getaddrinfo`](https://www.freebsd.org/cgi/man.cgi?query=getaddrinfo&sektion=3), [`gethostbyname`](https://www.freebsd.org/cgi/man.cgi?query=gethostbyname&sektion=3)
- [`inet_aton`](https://www.freebsd.org/cgi/man.cgi?query=inet_aton&sektion=3), [`htonl`](https://www.freebsd.org/cgi/man.cgi?query=htonl&sektion=3)
