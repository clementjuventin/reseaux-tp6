#include <errno.h>
#include <err.h>
#include <sysexits.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <libgen.h>

#include "sqserv.h"

char *prog_name;

void usage() {
    printf("usage: %s [-p <port>]\n", prog_name);
    printf("\t-p <port> specify alternate port\n");
}

void read_arguments(long *port, int argc, char* argv[]){
    long ch;
    /* get options and arguments */
    prog_name = strdup(basename(argv[0]));
    while ((ch = getopt(argc, argv, "?hp:")) != -1) {
        switch (ch) {
            case 'p': *port = strtol(optarg, (char **)NULL, 10);break;
            case 'h':
            case '?':
            default: usage(); return;
        }
    }
    argc -= optind;
    argv += optind;

    if (argc != 0) {
        usage();
        return;
    }
}

void init_socket(int *sock, struct sockaddr_in *sin, int port, int max_connections){
    /* create and bind a socket */
    *sock = socket(AF_INET, SOCK_STREAM, 0);
    if (*sock < 0) {
        err(EX_SOFTWARE, "in socket");
    }

    /* Init sin */
    memset(sin, 0, sizeof(*sin));
    sin->sin_family = AF_INET;
    sin->sin_addr.s_addr = INADDR_ANY; 
    sin->sin_port = htons(port);
    /* Bind */
    int bind_res = bind(*sock, (struct sockaddr *) sin, sizeof(*sin));
    if (bind_res < 0) {
        close(*sock);
        err(EX_SOFTWARE, "in bind");
    }
    /* listen data */
    int listen_res = listen(*sock, max_connections);
    if (listen_res < 0) {
        close(*sock);
        err(EX_SOFTWARE, "in listen");
    }
}

int main(int argc, char* argv[]) {

    long port = DEFAULT_PORT;
    struct sockaddr_in sin;
    socklen_t sin_len;
    int sock;

    int MAX_CONNECTIONS = 5;

    debug("debug is on\n");

    read_arguments(&port, argc, argv);
    init_socket(&sock, &sin, port, MAX_CONNECTIONS);

    printf("listening on port %d - accept %d connections\n", (int)port, MAX_CONNECTIONS);

    sin_len = sizeof(sin);
    int max_sd;
    int new_socket , client_socket[5], activity , sd;
    //set of socket descriptors 
    fd_set readfds;

    for(int i =0; i<MAX_CONNECTIONS; i++){
        client_socket[i] = 0;
    }
    puts("Waiting for connections ...");
    while(1==1)  
    {  
        //clear the socket set 
        FD_ZERO(&readfds);  
     
        //add master socket to set 
        FD_SET(sock, &readfds);  
        max_sd = sock;  
            
        ///////////////////////////////////////// INIT CLIENT SOCKETS
        //add child sockets to set 
        for (int i = 0 ; i < MAX_CONNECTIONS ; i++)  
        {  
            //socket descriptor 
            sd = client_socket[i];  
                 
            //if valid socket descriptor then add to read list 
            if(sd > 0)
                FD_SET(sd, &readfds);  
                 
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }
        //////////////////////////////////////// WAIT FOR CONNECTIONS
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
        if ((activity < 0) && (errno!=EINTR))  
        {  
            printf("select error");  
        }
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(sock, &readfds))  
        {  
            if ((new_socket = accept(sock,(struct sockaddr *)&sin, &sin_len))<0)  
            {  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
             
            //inform user of socket number - used in send and receive commands 
            printf("New connection, socket fd is %d, ip is : %s, port : %d\n" , new_socket , inet_ntoa(sin.sin_addr) , ntohs(sin.sin_port));  
                                  
            //add new socket to array of sockets 
            for (int i = 0; i < MAX_CONNECTIONS; i++)  
            {  
                //if position is empty 
                if( client_socket[i] == 0 )  
                {  
                    client_socket[i] = new_socket;  
                    printf("Adding to list of sockets as %d\n" , i);  
                         
                    break;  
                }  
            }  
        }
    }
    return EX_OK;
}
