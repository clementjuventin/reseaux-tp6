#include <errno.h>
#include <err.h>
#include <sysexits.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <libgen.h>

#include "sqserv.h"

char *prog_name;

void usage() {
    printf("usage: %s [-p <port>]\n", prog_name);
    printf("\t-p <port> specify alternate port\n");
}

int main(int argc, char* argv[]) {

    long port = DEFAULT_PORT;
    struct sockaddr_in sin;
    int sock;
    size_t  data_len;
    ssize_t len;
    char *data, *end;
    long ch;

    debug("debug is on\n");

    /* get options and arguments */
    prog_name = strdup(basename(argv[0]));
    while ((ch = getopt(argc, argv, "?hp:")) != -1) {
        switch (ch) {
            case 'p': port = strtol(optarg, (char **)NULL, 10);break;
            case 'h':
            case '?':
            default: usage(); return 0;
        }
    }
    argc -= optind;
    argv += optind;

    if (argc != 0) {
        usage();
        return EX_USAGE;
    }

    printf("listening on port %d\n", (int)port);

    /* get room for data */
    data_len = BUFFER_SIZE;
    if ((data = (char *) malloc(data_len)) < 0) {
        err(EX_SOFTWARE, "in malloc");
    }

    /* create and bind a socket */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        free(data);
        err(EX_SOFTWARE, "in socket");
    }

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY; // rather memcpy to sin.sin_addr
    sin.sin_port = htons(port);

    if (bind(sock, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
        free(data);
        close(sock);
        err(EX_SOFTWARE, "in bind");
    }

    /* listen data */
    if (listen(sock, 1) < 0) {
        free(data);
        close(sock);
        err(EX_SOFTWARE, "in listen");
    }

    struct sockaddr_in csin = { 0 };

    socklen_t csin_size = sizeof(csin);

    int csock = accept(sock, (struct sockaddr *)&csin, &csin_size);

    if (csock < 0) {
        free(data);
        err(EX_SOFTWARE, "in socket");
    }

    printf("connection accepted\n");

    /* receive data */
    len = recv(csock, data, data_len, 0);
    if (len < 0) {
        free(data);
        close(csock);
        err(EX_SOFTWARE, "in recv");
    }

    printf("got '%s' from IP address %s port %d\n", data, inet_ntoa(csin.sin_addr), ntohs(csin.sin_port));

    ch = strtol(data, &end, 10);

    switch (errno) {
        case EINVAL: err(EX_DATAERR, "not an integer");
        case ERANGE: err(EX_DATAERR, "out of range");
        default: if (ch == 0 && data == end) errx(EX_DATAERR, "no value");  // Linux returns 0 if no numerical value was given
    }

    /* send data */
    long square = ch * ch;
    char str[10];

    printf("integer value received: %ld\n", ch);
    printf("integer value to reply: %ld\n", square);
    
    /* int to str */
    sprintf(str, "%ld", square);
    
    // int res = sendto(int s, const void *msg, size_t len, unsigned int flags, struct sockaddr *from, socklen_t *fromlen);
    int res = send(csock, &str, strlen(str), 0);

    printf("reply sent, res: %d\n", res);

    /* cleanup */
    free(data);
    close(csock);

    return EX_OK;
}
